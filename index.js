fs = require('fs');
const express = require("express"),
    routes = require("./routes"),
    cors = require("cors"),
    bodyParser = require("body-parser"),
    app = express(),
    hubUtility = require('./lib/hubUtility')
    config = require('./config');

const privateKey = fs.readFileSync('/etc/letsencrypt/live/isure.ng/privkey.pem', 'utf8')
const certificate = fs.readFileSync('/etc/letsencrypt/live/isure.ng/cert.pem', 'utf8')
const ca = fs.readFileSync('/etc/letsencrypt/live/isure.ng/fullchain.pem', 'utf8')

const sslOptions = {
    cert: certificate,
    ca: ca,
    key: privateKey
}
  

let server
if (!config.enableSSL) server = require('http').Server(app)
else server = require('https').Server(sslOptions, app)


// app.listen(3000, () => {
//     console.log("server listening on port 3000");
// });

server.listen(config.serverPort, () => {
    console.log(`server listening on port ${config.serverPort}`);
})

app.use(bodyParser.json());    
app.use(cors());
app.use(express.urlencoded({ extended: true }));

routes(app)
hubUtility.scheduleHubBookings()

