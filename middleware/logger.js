const logger = require('../lib/log')

module.exports = function (req, res, next) {
  logger.warn(`*****************CALL: ${req.path} ********************`)
  next()
  logger.warn(`*****************END: ${req.path} ********************`)
}
