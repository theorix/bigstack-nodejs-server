const path = require('path')
const dotenv = require('dotenv')

dotenv.config(
  {
    path: path.join(__dirname, '../.env')
  }
)

module.exports = {
  db: {
    host: process.env.MYSQL_HOST,
    user: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DATABASE
  },
  tokenPrivateKey: process.env.TOKEN_PRIVATE_KEY,
  location: {
    longitude: Number(process.env.LONGITUDE),
    latitude: Number(process.env.LATITUDE),
    errorLimit: Number(process.env.LOCATION_ERROR_LIMIT)
  },
  hubWorkSpace: {
    basic: Number(process.env.BASIC_WORKSPACE),
    executive: Number(process.env.EXECUTIVE_WORKSPACE),
    numberOfDaysInAWeek: Number(process.env.NUMBER_OF_DAYS_IN_A_WEEK),
    priceOfBasic1: Number(process.env.PRICE_OF_BASIC_1),
    priceOfBasic2: Number(process.env.PRICE_OF_BASIC_2),
    priceOfExecutive1: Number(process.env.PRICE_OF_EXECUTIVE_1),
    priceOfExecutive2: Number(process.env.PRICE_OF_EXECUTIVE_2),
    priceDiscount: Number(process.env.PRICE_DISCOUNT)
  },
  serverPort: Number(process.env.SERVER_PORT),
  enableSSL: Number(process.env.ENABLE_SSL),

  debugMode: parseInt(process.env.DEBUG_MODE)
}
