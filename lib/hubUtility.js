var cron = require('node-cron')
const axios = require('axios')
const Model = require('../models')
const config = require('../config')
const dateUtility = require('./dateUtility')
const user = require('../models/user')
const booking = require('../models/booking')
const dateStringFormat = dateUtility.dateStringFormat

let inited = false
let numberOfRuns = 0
let numberOfBasicWorkspace = config.hubWorkSpace.basic
let numberOfExecutiveWorkspace = config.hubWorkSpace.executive
const bookingCache = [] // holds temporarily all booking in memory for faster retrieval
let activeDate = dateStringFormat(new Date())

// function to initialize the booking Cache
 async function initBookingCache() {
    let allBookingByDate = await Model.HubBooking.get({
      $or: [{subscription_status: 'Active'}, {subscription_status: 'Pending'}],
      $ext:{
        $orderby: {
          key: 'end_date',
          'order': 'DESC'
        }
      }
    })

    const highestBookingByDate = allBookingByDate[0]
    if (!highestBookingByDate) return;
    const today = new Date()
    const diffDays = dateUtility.dateDiffs(today, highestBookingByDate.end_date)
    for(let n = 0; n < diffDays; n++) {
      let date = dateUtility.addDays(today, n)
      let day = dateStringFormat(date)
      if (!dateUtility.workDay(date)) {
        console.log('WEEKEND....', day)
        continue
      }
      let thisDate = {}
      thisDate[day] = []
      for(let i = allBookingByDate.length - 1; i >= 0; i--) {
        const booking = allBookingByDate[i]
        let startDate = dateStringFormat(booking.start_date)
        let endDate = dateStringFormat(booking.end_date)
        if (day >= startDate && day <= endDate) thisDate[day].push(booking)
      }
      bookingCache.push(thisDate)
    }
    console.log(bookingCache, diffDays, bookingCache.length)
 }

 // function to add booking
 async function addBooking(booking, bookingData) {
  if (!inited) await initScheduler()

  const startDate = dateStringFormat(booking.start_date)
  const endDate = dateStringFormat(booking.end_date)
  const numberOfWorkspace = booking.hub_package.includes('Basic') ? numberOfBasicWorkspace : numberOfExecutiveWorkspace
  let wallet;
  
  // logic for adding into cache
  let diffDays = dateUtility.dateDiffs(startDate, endDate)
  let n = 0 // number of successfull bookings
  let m = 0
  const bookingPrice = bookingData.price
  const ignoreWallet = bookingData.ignore_wallet
  let failedBookings = [];
  let foundDateInCache = true
  let day;

  console.log('Booking:', bookingData)
  // delete booking['price']
  // delete booking['ignoreWallet']
  // console.log('boooking:', booking)

  // check wallet balance
  if (!ignoreWallet) {
    const user = await Model.User.fetch({ email: booking.email })
    if (!user.length) return {success: false, message: 'Invalid User'}
    wallet = (await Model.UserWallet.get({ user_id: user[0].id }))[0]
    console.log("Wallet:", wallet)
    const totalAmount = (diffDays + 1) * bookingPrice
    if (!wallet || Number(wallet.balance) < totalAmount) return {success: false, message: 'Insufficient funds'}
  }

  for(; m <= diffDays && foundDateInCache; m++) {
    let date = dateUtility.addDays(booking.start_date, m)
    day = dateStringFormat(date)
    if (!dateUtility.workDay(date)) {
      console.log('WEEKEND....', day)
      failedBookings.push({
        day,
        description: 'Weekend'
      })
      diffDays++  // add replace weekend
      continue
    }
    foundDateInCache = false
    for(let i = 0; i < bookingCache.length; i++) {
      const bookings = bookingCache[i]
      if (bookings[day]) {
        foundDateInCache = true
        if (bookings[day].length < numberOfWorkspace) { // there is still vacancy for this day
          bookingCache[i][day].push(booking)
          n++
        } else if(bookings[day]) {  // there is no vacancy for this day
          console.log(`THERE IS NO VACANCY ON ${day}`)
          failedBookings.push({
            day,
            description:'No Vacancy'
          })
        }
        break
      }
    }
    if(!foundDateInCache) {  // add new entry for 'day' to cache
      let d = {}
      d[day] = [booking]
      bookingCache.push(d)
    }
  }
  // add remaining days into cache
  if (bookingCache.length < diffDays) {
    for(; m < diffDays; m++) {
      let date = dateUtility.addDays(booking.start_date, m)
      day = dateStringFormat(date)
      if (!dateUtility.workDay(date)) {
        console.log('WEEKEND....', day)
        failedBookings.push({
          day,
          description: 'Weekend'
        })
        diffDays++  // add replace weekend
        continue
      }
      let thisDay = {}
      thisDay[day] = [booking]
      bookingCache.push(thisDay)
      n++
    }
  }

  // activate booking if necessary
  if (startDate === activeDate) {
    booking.subscription_status = 'Active';
    // bookingCache[0][activeDate].push(booking)
  }
  booking.end_date = day
  if(!ignoreWallet) {
    wallet.balance = Number(wallet.balance) - (n * bookingPrice)
    wallet.balance = wallet.balance + (n > 4 ? n * config.hubWorkspace.priceDiscount : 0)
    await wallet.save()
  }
  await booking.save()



  let summary = booking.toJson()
  summary.failedBookings = failedBookings
  summary.totalAmount = n * bookingPrice

  console.log('Bookings2:', bookingCache)

  // return n > 0 ? summary : false
  return {
    success: n > 0 ? true : false,
    message: n === 0 ? 'No hub space available' : '',
    data: summary
  }
}

async function initScheduler () {
  await initBookingCache()
  inited = true
  console.log('SCHEDULER INITED')
}


module.exports = {
  scheduleHubBookings: function () {
    cron.schedule(`* * * * *`, async () => {
      // `0 0 * * *`
      console.log('RUN', ++numberOfRuns)
      if (!inited) await initScheduler()
      
      let today = dateStringFormat(new Date())
      let topBookings = bookingCache[0]
      if(!topBookings) return;
      if(topBookings[activeDate]) {  // there where active booking yesterday
        for (let booking of topBookings[activeDate]) {
          if (dateStringFormat(booking.end_date) < today) {
            booking.subscription_status = 'Expired'
            await booking.save()
          }
        }

        // remove the all bookings for yesterday
        if(today !== activeDate) bookingCache.shift()
        console.log('Bookings:', bookingCache)
      }

      // activate bookings that begins today
      topBookings = bookingCache[0]
      if(topBookings[today]) {
        for (let booking of topBookings[today]) {
          if (dateStringFormat(booking.start_date) === today) {
            booking.subscription_status = 'Active'
            await booking.save()
          }
        }
      }

      // set the current active date
      activeDate = today
    })
  },

  bookWorkspace: async function(data) {
    return new Promise(async (resolve, reject) => {
      Model.HubBooking.startTransaction()
        .then((transaction) => {
          let bookingData = JSON.parse(JSON.stringify(data))
          delete data['price']
          delete data['ignore_wallet']
          Model.HubBooking.create(data)
          .then(async (bookingId) => {
            let booking = (await Model.HubBooking.get({ id: bookingId }))[0]
            let bookingResult = await addBooking(booking, bookingData)
            if (bookingResult.success) {
              await transaction.commit()
              resolve(bookingResult.data)
            } else {
              await transaction.rollback()
              reject(new Error(bookingResult.message))
            }
            
          })
          .catch(async err => {
            console.log(err);
            await transaction.rollback()
            reject(new Error('Invalid data supplied'))
          });
        })
    })
  },

  checkIn: async function(userEmail, staffId) {
    return new Promise((resolve, reject) => {
      if(!userEmail) return reject(new Error('Invalid User Email'))
      if(!staffId || isNaN(staffId)) return reject(new Error('Inavlid Staff Id'))
      const todayBookings = bookingCache[0][activeDate]
      for(booking of todayBookings) {
        if(booking.email === userEmail) {
          Model.hubCheckin.create({
            booking_id: booking.id
          }).then((checkInId)=> {
            resolve(booking.toJson())
          }).catch((error)=>{
            reject(error)
          })
        }
      }
      reject(new Error(`No Active Booking for ${email}`))
    })
  }
}
