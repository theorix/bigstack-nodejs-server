var QRCode = require('qrcode')
 

// QRCode.toString('Bigstack Tecnologies',{type:'terminal'}, function (err, url) {
//     console.log(url)
//   })

module.exports = {
    generate: function (strData) {
        return new Promise((resolve, reject) => {
            if (!strData) return reject(new Error('Invalid Input'))
            QRCode.toDataURL(strData, function (err, url) {
                if (err) return reject(err)
                // console.log(url)
                resolve(url)
              })
        })
    }
}