const mysql = require('mysql')
const Database = require('./prototype')
const keyHandler = require('../db/query')
const appConfig = require('../config').getConfig()
const errorHandler = new (require('../error/ErrorHandler'))('MySQL Driver')

class MySQL extends Database {
  constructor () {
    super()
    this.db = mysql.createConnection({
      connectTimeout: 0,
      host: appConfig.db.mysql.host,
      user: appConfig.db.mysql.user,
      password: appConfig.db.mysql.password,
      database: appConfig.db.mysql.database,
      charset: 'UTF8_UNICODE_CI',
      stringifyObjects: false,
      supportBigNumbers: true,
      bigNumberStrings: true,
      timezone: 'UTC',
      dateStrings: false
    })
    this.connected = false

    /* errorHandler.on('error-log', (err)=> {
      console.log(err.message)
    }) */
  }

  // connection to db
  connect () {
    let instance = this
    this.db.connect(function (err) {
      if (err) throw err
      console.log(` [PID=${process.pid}]`, 'Connected to MySQL DB!')
      instance.connected = true
    })
  }

  // disconnect from db
  disconnect () {
    this.connected = false
    console.warn(' [*] Disconnected from MySQL database!')
  }

  // get the instance of the db for raw query processing
  getInstance () {
    return this.db
  }

  /**
   *  This function fetches data from db.
   * @param {Object} options - the parameter has the following form:
    *   options =====> { table:  table_name,
    *                    field: single_field_string | [field1, field2, ...],
    *                    where: { key_field_1: value_1, key_field_2: value_2, ... }
    *                   }
    */
  fetch (options) {
    let fieldList

    // determine properties given in options
    if (typeof options.field === 'string' || options.field instanceof String) {
      fieldList = options.field
    } else if (Array.isArray(options.field)) { // make a field list from the array of fields
      fieldList = options.field.toString()
    } else {
      errorHandler.log(
        { message: 'Bad argument(s)' },
        { methodName: 'MySQL.fetch' }
      )
    }

    // construct the query based on user options
    let where = ''
    let queryOptions = {}
    if (options.join) {
      where = this.interpretWhereClause(options.where, options.join)
      queryOptions.nestTables = true
    } else where = this.interpretWhereClause(options.where, {})

    let instance = this

    return new Promise((resolve, reject) => {
      queryOptions.sql = `SELECT ${fieldList} FROM ${appConfig.db.mysql.database}.${options.table} ${where}`
      if (appConfig.debug) {
        console.log('SQL:', queryOptions.sql)
      }
      instance.db.query(queryOptions, function (err, results, fields) {
        if (err) {
          errorHandler.log(
            err,
            { methodName: 'MySQL.fetch' }
          )
          errorHandler.log(err)
          reject(err)
        } else {
          resolve({ results: results, fields: fields })
        }
      })
    })
  }

  // fetch data from db, making sure InnoDB locks the row
  /**
   *  @param {Object} options - the parameter has the following form:
    *   options =====> { table: table_name,
    *                    field: single_field_string | [field1, field2, ...],
    *                    where: { key_field_1: value_1, key_field_2: value_2, ... }
    *                   }
    */
  fetchForUpdate (options) {
    let fieldList

    // determine properties given in options
    if (typeof options.field === 'string' || options.field instanceof String) {
      fieldList = options.field
    } else if (Array.isArray(options.field)) { // make a field list from the array of fields
      fieldList = options.field.toString()
    } else {
      errorHandler.log(
        { message: 'Bad argument(s)' },
        { methodName: 'MySQL.fetchForUpdate' }
      )
    }

    // construct the query based on user options
    let where = ''
    if (options.join) { q = this.interpretWhereClause(options.where, options.join) } else where = this.interpretWhereClause(options.where, {})

    let instance = this

    return new Promise((resolve, reject) => {
      instance.db.query(
        `SELECT ${fieldList} FROM ${appConfig.db.mysql.database}.${options.table} ${q} FOR UPDATE`,
        function (err, results, fields) {
          if (err) {
            errorHandler.log(
              err,
              { methodName: 'MySQL.fetchForUpdate' }
            )
            errorHandler.log(err)
            reject(err)
          } else {
            resolve({ results: results, fields: fields })
          }
        })
    })
  }

  count (options) {
    let fieldList

    // determine properties given in options
    if (typeof options.field === 'string' || options.field instanceof String) {
      fieldList = options.field
    } else if (Array.isArray(options.field)) { // make a field list from the array of fields
      fieldList = options.field.toString()
    } else {
      errorHandler.log(
        { message: 'Bad argument(s)' },
        { methodName: 'MySQL.fetch' }
      )
    }

    // construct the query based on user options
    let q = ''
    if (options.join) {
      q = this.interpretWhereClause(options.where, options.join)
    } else q = this.interpretWhereClause(options.where, {})

    const instance = this
    return new Promise((resolve, reject) => {
      instance.db.query(`SELECT COUNT(${fieldList}) AS count FROM ${appConfig.db.mysql.database}.${options.table} ${q}`, function (err, results, fields) {
        if (err) {
          errorHandler.log(
            err,
            { methodName: 'MySQL.count' }
          )
          errorHandler.log(err)
          reject(err)
        } else {
          resolve(results[0]['count'])
        }
      })
    })
  }

  /**
   * Given suitable data, update db with such data.
   *  @param {Object} options - has the following form:
    *   options =====> { table: table_name,
    *                    field: [ { field1:value1 }, { field2:value2 }, ... ],
    *                    where: { key_field_1: value_1, key_field_2: value_2, ... }
    *                   }
    */
  update (options) {
    let table = options.table
    let fields = options.field
    let conditions = ''

    if (options.join) {
      conditions = this.interpretWhereClause(options.where, options.join)
    } else conditions = this.interpretWhereClause(options.where, {})
    const instance = this
    let v = ''; let V; let q

    for (let i = 0; i < fields.length; i++) {
      for (let k in fields[i]) {
        if ((i + 1) < fields.length) v += `${k} = ${mysql.escape(fields[i][k])}, `
        else v += `${k} = ${mysql.escape(fields[i][k])}`
      }
    }

    V = `${v}`
    q = `UPDATE ${appConfig.db.mysql.database}.${table} SET ${V} ${conditions}`
    if (appConfig.debug) {
      console.log('SQL:', q)
    }
    return new Promise((resolve, reject) => {
      instance.db.query(q, function (err, results) {
        if (err) {
          errorHandler.log(
            err,
            { methodName: 'MySQL.update' }
          )
          reject(err)
        } else resolve(true)
      })
    })
  }
  /**
   * This function is responsible for creating (or adding) new data to db
   * @param {Object} details - has the following format:
   *    details =====> {
   *              table: table_name,
   *              data: [ { field1:value1 }, { field2:value2 }, ... ]
   *        }
   */
  add (details) {
    let table = details.table
    let data = details.data
    let instance = this
    let values = '('
    let keys = '('

    for (let i = 0; i < data.length; i++) {
      for (let key in data[i]) {
        if ((i + 1) < data.length) {
          keys += `${key},`
          values += `${mysql.escape(data[i][key])},`
        } else {
          keys += `${key}`
          values += `${mysql.escape(data[i][key])}`
        }
      }
    }

    values += ')'
    keys += ')'

    const q = `INSERT INTO ${appConfig.db.mysql.database}.${table}${keys} VALUES ${values}`
    if (appConfig.debug) {
      console.log('SQL:', q)
    }
    return new Promise((resolve, reject) => {
      instance.db.query(q, function (err, result) {
        if (err) {
          console.log(err.message)
          errorHandler.log(
            err,
            { methodName: 'MySQL.add' }
          )
          reject(err)
        } else resolve(result.insertId)
      })
    })
  }

  interpretWhereClause (where, join) {
    let q = ''
    let joinStr = ''
    if (!where) q = ' WHERE 1 = 1'
    else q = ' WHERE '

    if (!join) {
      joinStr = ''
    } else {
      if (join.type === 'join' || join.type === 'inner-join') joinStr = ` INNER JOIN ${join.rightTable} ON ${join.on}`
      else if (join.type === 'left-join') joinStr = ` LEFT JOIN ${join.rightTable} ON ${join.on}`
    }

    q += keyHandler.compileWhereClause(where)
    return joinStr + q
  }

  /**
   *  DELETE funtion
    *   options =====> { table: table_name,
    *                    where: { key_field_1: value_1, key_field_2: value_2, ... }
    *                   }
    * @param {Object} options - has the following format:
    */
  delete (options) {
    const condition = this.interpretWhereClause(options.where, {})
    const q = `DELETE FROM ${appConfig.db.mysql.database}.${options.table} ${condition}`
    if (appConfig.debug) {
      console.log('SQL:', q)
    }
    const instance = this

    return new Promise((resolve, reject) => {
      if (condition !== ' WHERE 1 = 1') {
        instance.db.query(q, function (err, result) {
          if (err) {
            return reject(err)
          }
          resolve(true)
        })
      } else resolve(false)
    })
  }

  transaction () {
    let instance = this

    return new Promise((resolve, reject) => {
      instance.db.beginTransaction((err) => {
        if (err) {
          errorHandler.log(err)
          reject(err)
        } else {
          resolve({
            commit: () => {
              return new Promise(resolve => {
                try {
                  instance.db.commit(() => {
                    resolve(true)
                  })
                } catch (err) {
                  reject(err)
                }
              })
            },
            rollback: () => {
              return new Promise(resolve => {
                try {
                  instance.db.rollback(() => {
                    resolve(true)
                  })
                } catch (err) {
                  reject(err)
                }
              })
            }
          })
        }
      })
    })
  }
}

module.exports = MySQL
