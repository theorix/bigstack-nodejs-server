const appConfig = require('../config').getConfig()
const Database = require(`../drivers/${appConfig.db.driver}`)
const db = new Database()

db.connect()

module.exports = db
