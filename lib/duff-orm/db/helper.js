// database helper functions

const dbFunctions = {
  toDataArray: function (data) {
    let dataArray = []
    for (let k in data) {
      let d = {}
      d[k] = data[k]
      dataArray.push(d)
    }

    return dataArray
  },

  regReplace: function (regStr, targetStr ,str) {
    if (typeof str === 'string') {
      return str.replace(new RegExp(regStr, 'g'), targetStr)
    }
    return str
  }
}

module.exports = dbFunctions
