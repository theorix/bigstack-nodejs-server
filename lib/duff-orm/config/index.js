let configuration = null

module.exports = {
  configure: function (config) {
    configuration = config
  },

  getConfig: function () {
    return configuration
  }
}
/*
module.exports = {

  db: {
    driver: 'mysql',
    mysql: {
      table: {
        products: 'products'
      },
      host: 'localhost',
      user: 'root',
      password: 'mr cheat',
      database: 'api_db',
    }
  },
  debugMode: true
}
*/
