module.exports = {
    configure: function (config) {
    // configure orm
    require('./config').configure(config)
  },

  model: function () {
    return require('./model')
  }
}
