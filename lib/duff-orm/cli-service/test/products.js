 module.exports = {
 "config": {
  "tableName": "products",
  "primaryKey": "id"
 },
 "fields": [
  "id",
  "name",
  "description",
  "price",
  "category_id",
  "created",
  "modified"
 ]
}