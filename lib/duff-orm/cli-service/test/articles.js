 module.exports = {
 "config": {
  "tableName": "articles",
  "primaryKey": "id"
 },
 "fields": [
  "id",
  "coverImage",
  "post_date",
  "title",
  "author",
  "comments",
  "content",
  "views",
  "approved",
  "approved_by"
 ]
}