 module.exports = {
 "config": {
  "tableName": "users",
  "primaryKey": "id"
 },
 "fields": [
  "id",
  "first_name",
  "last_name",
  "email",
  "password",
  "admin",
  "active"
 ]
}