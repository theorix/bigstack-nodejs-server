 module.exports = {
 "config": {
  "tableName": "categories",
  "primaryKey": "id"
 },
 "fields": [
  "id",
  "name",
  "description",
  "created",
  "modified"
 ]
}