 module.exports = {
 "config": {
  "tableName": "gallery",
  "primaryKey": "id"
 },
 "fields": [
  "id",
  "imgSrc",
  "event_date",
  "title"
 ]
}