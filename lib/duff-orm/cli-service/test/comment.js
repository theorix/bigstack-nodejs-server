 module.exports = {
 "config": {
  "tableName": "comment",
  "primaryKey": "id"
 },
 "fields": [
  "id",
  "article_id",
  "content",
  "author",
  "comment_date",
  "approved",
  "approved_by"
 ]
}