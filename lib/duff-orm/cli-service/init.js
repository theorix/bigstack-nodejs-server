const readline = require('readline')
const fsHelper = require('./fsHelper')
const cli = require('./cli')
const mysql = require('mysql')

function firstCap(str) {
  if(typeof str !== 'string') return ''
  return str.charAt(0).toUpperCase() + str.slice(1)
}

function cabebCase(str) {
  if(typeof str !== 'string') return ''
  let words = str.split('_')
  let cabeb = ''
  for(let word of words) {
    cabeb += firstCap(word)
  }
  return cabeb
}


const configData = ''
if(fsHelper.pathExists('duff-orm.json')) {
  fsHelper.readFile('duff-orm.json')
    .then((configData) => {
      db = mysql.createConnection({
        connectTimeout: 0,
        host: configData.db.mysql.host,
        user: configData.db.mysql.user,
        password: configData.db.mysql.password,
        database: 'information_schema',
        charset: 'UTF8_UNICODE_CI',
        stringifyObjects: false,
        supportBigNumbers: true,
        bigNumberStrings: true,
        timezone: 'UTC',
        dateStrings: false
      })

      db.query(`SELECT TABLE_NAME, TABLE_SCHEMA FROM TABLES WHERE TABLE_SCHEMA = "${configData.db.mysql.database}"`,
      function(err, results, fields) {
        if(err) throw(err)
        // use the table name to create each file
        let model = ''
        for(let t of results) {
          // fetch each table columns here
          db.query(`SELECT COLUMN_NAME, COLUMN_KEY, TABLE_SCHEMA, TABLE_NAME FROM COLUMNS WHERE TABLE_SCHEMA = "${configData.db.mysql.database}" AND TABLE_NAME = "${t.TABLE_NAME}"`,
            function(er,res, f) {
              // columns
              let columns = []
              let primaryKey = ''
              for(let c of res) {
                columns.push(c.COLUMN_NAME)
                if(c.COLUMN_KEY === 'PRI') primaryKey = c.COLUMN_NAME
              }

              // prepare model data
              model = {}
              model['config'] = {
                tableName: t.TABLE_NAME,
                primaryKey
              }
              model['fields'] = columns
              model['override'] = {}
              model['override']['staticMethods'] = {}
              model['override']['classMethods'] = {}

              // create the model files
              fsHelper.writeToFile(`${cabebCase(t.TABLE_NAME)}.js`, ` module.exports = ${JSON.stringify(model, null, 1)}`)
              .then((success) => {
                console.log('done writing:', `${cabebCase(t.TABLE_NAME)}.js`)
                // console.log('CURRENT WORKING DIRECTORY:', process.cwd())
              }).catch((err) => {
                console.log(err.message)
              })

            }
          )
        }
      }
    )

      console.log('done')
    })
}

cli.init()


// User.Query(``)
