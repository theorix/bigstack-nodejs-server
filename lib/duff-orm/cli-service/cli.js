const readline = require('readline')
const events = require('events')
class _events extends events {}
const e = new _events()

let cli = {}

// input handlers
e.on('man', function(str){
  cli.responders.help()
})

e.on('help', function(str){
  cli.responders.help()
})

e.on('exit', function(str){
  cli.responders.exit()
})

e.on('show config', function(str){
  cli.responders.showConfig()
})

// Responders Object
cli.responders = {}

cli.responders.help = function() {
  console.log('You asked for help')
}

cli.responders.exit = function() {
  console.log('Bye!')
  process.exit(0)
}

cli.responders.showConfig = function() {
  console.log('You want to show config')
}



cli.processInput = function (str) {
  str = typeof(str) == 'string' && str.trim().length > 0 ? str.trim() : false

  if(str) {
    // list of strings allowed in this cli
    let uniqueInputs = [
      'man',
      'help',
      'exit',
      'show config'
    ]

    // go through list and emit event when a match is found
    let matchFound = false
    let counter = 0
    uniqueInputs.some((input)=>{
      if(str.toLowerCase().indexOf(input) > -1) {
        matchFound = true
        e.emit(str, input)
        return true
      }
    })

    if(!matchFound) {
      console.log('sorry, no match found!!')
    }
  }
}

// init script
cli.init = function () {
  // start a cli interface
  cli_interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt: '>> '
  })

  // creates initial prompt
  cli_interface.prompt()

  // handle each line of input separately
  cli_interface.on('line', (str) => {
    // send the input to the appropriate process
    cli.processInput(str)

    // re-initializ prompt
    cli_interface.prompt()
  })

  // if user stops the CLI, kill the associate process
  cli_interface.on('close', () => {
    process.exit(0)
  })
}

module.exports = cli
