const db = require('../db')
const dbHelper = require('../db/helper')
const whereCompiler = require('../db/query')
const config = require('../config').getConfig()
// const EventEmitter = require('events').EventEmitter
const EventEmitter = require('../lib/duff-observable')

module.exports = function modelGenerator (data) {
  // create constructor
  let Model = function (modelData) {
    // populate constructor
    for (let d in modelData) {
      this[d] = modelData[d]
    }

    // this is necessary so to prevent adding unnecessary update event handlers
    // this property is only set to true when any update method e.g save() is called
    this._subscribeForUpdate = false // means that this object has no update notification enabled
    this._updatedData = {} // the update data sent to other objects
    this._joinMode = false // means that no joins should be used in Model.get()
  }

  // attach an event emitter (A 'subject' in Observer design pattern jargon)
  Model.eventEmitter = new EventEmitter(true) // enable getLatest update

  // utility methods
  Model.prototype.getEventEmitter = function () {
    return Model.eventEmitter
  }

  // ************* static methods ***********************
  Model.get = function (where) { // get without joins
    return new Promise((resolve, reject) => {
      db.fetch({ table: data.config.tableName, field: '*', where })
        .then((dataFetched) => {
          let files = []

          for (let i = 0; i < dataFetched.results.length; i++) {
            let file = new Model(dataFetched.results[i])
            files.push(file)
          }
          resolve(files)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  Model.fetch = function (where) { // get without joins
    return new Promise((resolve, reject) => {
      db.fetch({ table: data.config.tableName, field: '*', where })
        .then((dataFetched) => {
          let files = []

          for (let i = 0; i < dataFetched.results.length; i++) {
            let file = dataFetched.results[i]
            files.push(file)
          }
          resolve(files)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  Model.Get = function (where) { // get with joins
    return new Promise((resolve, reject) => {
      let join = ''
      if (data.relate) {
        join = {
          type: 'inner-join',
          rightTable: data.relate.table,
          on: `${data.relate.table}.${data.relate.on} = ${data.config.tableName}.${data.relate.with}`
        }
      }

      db.fetch({ table: data.config.tableName, field: '*', where, join })
        .then((dataFetched) => {
          let files = []
          let tmp = ''
          let subArr = []

          for (let i = 0; i < dataFetched.results.length; i++) {
            let row = dataFetched.results[i]
            if (tmp && tmp[data.config.primaryKey] !== row[data.config.tableName][data.config.primaryKey]) {
              let file = new Model(tmp)
              file[data.relate.table] = subArr
              files.push(file)
              subArr = []

              tmp = row[data.config.tableName]
              subArr.push(row[data.relate.table])
            } else {
              tmp = row[data.config.tableName]
              subArr.push(row[data.relate.table])
            }

            if (i === dataFetched.results.length - 1) {
              let file = new Model(row[data.config.tableName])
              file[data.relate.table] = subArr
              files.push(file)
            }
          }

          resolve(files)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  Model.getById = function (id) {
    return new Promise((resolve, reject) => {
      let idObj = {}
      idObj[data.config.primaryKey] = id
      Model.get(idObj).then((result) => {
        if (result.length > 0) resolve(result[0])
        else resolve(false)
      }).catch((err) => {
        reject(err)
      })
    })
  }

  Model.create = async function (modelData) {
    let dataArray = dbHelper.toDataArray(modelData)
    try {
      return await db.add({
        table: data.config.tableName,
        data: dataArray
      })
    } catch (err) {
      console.log(err.message)
      return false
    }
  }

  Model.update = function (modelData) {
    const that = this
    return new Promise((resolve, reject) => {
      const fields = dbHelper.toDataArray(modelData.set)
      db.update({ table: data.config.tableName,
        field: fields,
        where: modelData.queryKey,
        join: { type: '-' }
      })
        .then((result) => {
          // // 1. replace all AND, OR, NOT and EQUAL symbol of sql WHERE expression with JS's
          // let whereExpression = whereCompiler.compileWhereClause(modelData.queryKey)
          // whereExpression = dbHelper.regReplace('AND', '&&', whereExpression)
          // whereExpression = dbHelper.regReplace('OR', '||', whereExpression)
          // whereExpression = dbHelper.regReplace('<>', '!==', whereExpression)
          // whereExpression = dbHelper.regReplace(' = ', ' === ', whereExpression)

          // // 2. get the eventObjects of the event
          // const eventObjs = Model.eventEmitter.getEventObj()

          // // 3. Do the following:
          // for (let evt in eventObjs) { // for each event in the event object
          //   let tmpExpr = whereExpression
          //   if (eventObjs[evt].latestData) { // suppose this event has latestData field
          //     for (let f in eventObjs[evt].latestData) { // then for each field in latestData
          //       if (tmpExpr.includes(f)) { // check whether the field f is in the whereExpression
          //         // replace the field name f with the value of the latestData[f]
          //         tmpExpr = dbHelper.regReplace(f, eventObjs[evt]['latestData'][f].toString(), tmpExpr)
          //       }
          //     }
          //     // console.log('queryExpression:', eval(tmpExpr))
          //     if (eval(tmpExpr)) { // if the whereExpression evaluates to true
          //       for (let observer of eventObjs[evt].observers) { // then
          //         observer(modelData.set) // update all observers with the changes
          //       }
          //     }
          //   }
          // }

          // finally resolve to true
          resolve(true)
        })
        .catch((err) => {
          reject(err)
        })
    })
  }

  Model.delete = function (where) {
    return new Promise((resolve, reject) => {
      db.delete({
        where,
        table: data.config.tableName
      }).then((deleted) => {
        if (deleted) resolve(true)
        else resolve(false)
      }).catch((err) => {
        reject(err)
      })
    })
  }

  Model.count = function (where) {
    return new Promise((resolve, reject) => {
      db.count({
        table: data.config.tableName,
        field: '*',
        where
      }).then((c) => {
        resolve(c)
      }).catch((err) => {
        reject(err)
      })
    })
  }

  Model.searchQ = function (column, keyword) {
    if (keyword) {
      if (Array.isArray(column)) {
        return `MATCH(${column.join()}) AGAINST('${keyword}' IN BOOLEAN MODE)`
      } else {
        return `MATCH(${column}) AGAINST('${keyword}' IN BOOLEAN MODE)`
      }
    }
  }

  Model.search = function (column, keyword) {
    return new Promise((resolve, reject) => {
      const dbInstance = db.getInstance()
      if (keyword) {
        const like = Model.searchQ(column, keyword)

        let searchSql = `SELECT DISTINCT * FROM ${data.config.tableName} WHERE ${like}`
        console.log('SearchQuery:', searchSql)

        if (column) {
          dbInstance.query(searchSql, (err, results, fields) => {
            if (err) {
              reject(err)
            }
            resolve({
              results,
              fields
            })
          })
        } else {
          reject(new Error('Supply column name'))
        }
      } else {
        reject(new Error('Supply keyword'))
      }
    })
  }

  Model.getFields = function () {
    return data.fields
  }

  // raw query processing
  Model.Query = function (sql) {
    const dBInstance = db.getInstance()
    return new Promise((resolve, reject) => {
      dBInstance.query(sql, (err, results, fields) => {
        if (err) {
          reject(err)
        }
        resolve({
          results,
          fields
        })
      })
    })
  }

  // transaction
  Model.startTransaction = function () {
    return db.transaction()
  }

  // ******** End of Static Methods **********************

  // **************************** non - static methods*************************
  Model.prototype.toJson = function () {
    const jsonObj = {}
    for (let d in this) {
      if (typeof this[d] !== 'function' && d[0] !== '_') {
        jsonObj[d] = this[d]
      }
    }
    return jsonObj
  }

  Model.prototype.save = function () {
    const that = this
    return new Promise((resolve, reject) => {
      const fields = dbHelper.toDataArray(that.toJson())
      const where = {}
      where[data.config.primaryKey] = that[data.config.primaryKey]
      db.update({ table: data.config.tableName,
        field: fields,
        where,
        join: { type: '-' }
      })
        .then((result) => {
          // alert others to update
          that.getEventEmitter().emit(data.config.primaryKey, that._updatedData)
          that._updatedData = {} // reset the updated data
          resolve(true)
        })
        .catch((error) => {
          reject(error)
        })
    })
  }

  // get and set method
  Model.prototype.get = function (key) {
    return this[key]
  }

  Model.prototype.set = function (key, value) {
    if (data.fields.includes(key)) {
      // since we are updating we should send / receive update notification from other objects as well
      // if (!this._subscribeForUpdate) {
      //   this._subscribeForUpdate = true // means that this object has update notification enabled
      //   this.getEventEmitter().on(data.config.primaryKey, (d) => {
      //     // update this object based on the changes
      //     for (let field in d) {
      //       this[field] = d[field]
      //     }
      //   })
      // }
      // console.log('updated Data:', this.toJson())
      // do the setting here
      this[key] = value
      // this._updatedData[key] = value // this is the update data sent to other objects
    }
  }

  // ******** End of Non-Static Methods **************************

  // check for overrides
  if (data.override) {
    if (data.override.staticMethods) {
      for (let method in data.override.staticMethods) {
        if (typeof data.override.staticMethods[method] === 'function')
          Model[method] = data.override.staticMethods[method]
      }
    }

    if (data.override.classMethods) {
      for (let method in data.override.classMethods) {
        if (typeof data.override.classMethods[method] === 'function')
          Model.prototype[method] = data.override.classMethods[method]
      }
    }
  }

  return Model
}
