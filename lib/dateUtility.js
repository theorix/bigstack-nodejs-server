function workDay(date) {
  // getDay() ==> 0 = sunday, 1 = monday, 2 = tuesday, ... 6 = saturday
  if (date.getDay() === 6 || date.getDay() === 0) return false
  else return true
}


let dateDiffs = function (date1, date2, unit = 'days') {
    // unit = seconds || minutes || hours || days
    const dt1 = new Date(date1)
    const dt2 = new Date(date2)
    switch (unit) {
      case 'seconds':
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate(), dt2.getHours(), dt2.getMinutes(), dt2.getSeconds()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate(), dt1.getHours(), dt1.getMinutes(), dt1.getSeconds())) / (1000))
      case 'minutes':
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate(), dt2.getHours(), dt2.getMinutes(), dt2.getSeconds()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate(), dt1.getHours(), dt1.getMinutes(), dt1.getSeconds())) / (1000 * 60))
      case 'hours':
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate(), dt2.getHours(), dt2.getMinutes(), dt2.getSeconds()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate(), dt1.getHours(), dt1.getMinutes(), dt1.getSeconds())) / (1000 * 60 * 60))
      case 'days':
        return Math.floor((Date.UTC(dt2.getFullYear(), dt2.getMonth(), dt2.getDate()) - Date.UTC(dt1.getFullYear(), dt1.getMonth(), dt1.getDate())) / (1000 * 60 * 60 * 24))
    }
  }
  
  function addDays(startDate, days) {
    // const copy = new Date(Number(startDate))
    const copy = new Date(startDate)
    //console.log('ADD DAYS:', startDate, days, copy)
    copy.setDate(copy.getDate() + days)
    return copy
  }
  
  function daySequence(startDate) {
    if (!startDate) return ''
    let n = 0
  
    function nDay() {
      let nextDay = addDays(startDate, n++)
      if (!workDay(nextDay)) {
        console.log('Weekend...', n)
        return nDay()
      }
      return nextDay
    }
  
    return nDay
  }

// let day = daySequence(addDays(new Date(), 4))
// let N = 9 // days
// for (let n = 0; n < N; n++) 
//   console.log(n, ' : ',day().toDateString())


module.exports = {
    dateStringFormat: function (date) {
        if (!date) return ''
        date = typeof data === 'object' ? date : new Date(date)
        const yr = date.getFullYear()
        let month = Number(date.getMonth()) + 1
        let day = date.getDate()
    
        if (month < 10) month = `0${month}`
        if(day < 10) day = `0${day}`
        return `${yr}-${month}-${day}`
    },

    dateDiffs,
    addDays,
    daySequence,
    workDay
}