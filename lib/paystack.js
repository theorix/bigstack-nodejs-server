
// Require the library

// Download library from https://github.com/kehers/paystack
// var paystack = require('paystack')('sk_live_3c5812ee5daca9ba1c76a2e1f2904422a3444e2c')
var paystack = require('paystack')('sk_test_48634bf2f3ae37c0323cc9b4440d1001f6f7c5d8')

module.exports = {
  initialize: function(amount, email) {
    return new Promise((resolve, reject) => {
      paystack.transaction.initialize({
        amount,
        email
      }).then((resp) => {
        if(resp.status) resolve(resp.data)
        else reject(new Error('Invalid init'))
      }).catch((error) => {
        reject(new Error(error.message))
      })
    })
  },
  verify: function (txRef) {
    return new Promise((resolve, reject) => {
      paystack.transaction.verify(txRef, function (error, body) {
        if (error) {
          return reject(error)
        }
        resolve(true)
        console.log(body)
      })
    })
  }
}
