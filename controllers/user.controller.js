const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const Model = require("../models");
const config = require("../config");
const paystack = require("../lib/paystack");

module.exports = {
    register:  async (req, res) => {
        // validate the request body first
        if (!req.body.email || !req.body.name || !req.body.password) 
            return res.status(400).send('Invalid details');
      
        //find an existing user
        let user = await Model.User.get({ email: req.body.email });
        if (user.length) return res.status(400).send("User already registered.");
      
        userId = await Model.User.create({
          name: req.body.name,
          password: req.body.password,
          email: req.body.email
        });
        if (userId) {
            let user = await Model.User.getById(userId);
            user.password = await bcrypt.hash(user.password, 10);
            await user.save();
          
            const token = user.generateAuthToken();
            userObj = user.toJson()
            delete userObj['password']
            userObj.token = token
            res.header("x-auth-token", token).send(userObj);
        }
      },

      login: async function(req, res) {
        if (!req.body.email || !req.body.password) return res.status(400).send('Invalid Login details');
        const email = req.body.email
        let user = await Model.User.get({ email })
        if(user.length) {
            user = user[0]
            const passwordMatched = await bcrypt.compare(req.body.password, user.password);
            if (passwordMatched) {
                const token = user.generateAuthToken();
                userObj = user.toJson()
                if(!userObj.is_staff) {
                  const wallet = await Model.UserWallet.fetch({ user_id: user.id })
                  userObj['walletBalance'] = wallet.length ? wallet[0].balance : 0
                }
                delete userObj['password']
                userObj.token = token
                return res.header("x-auth-token", token).send(userObj);
            }
        }
        res.status(400).send("Invalide User Details.");
      },

      listUsers: function(req, res) {
          Model.User.fetch().then((users) => {
            users = users.map((user) => {
              delete user.password
              return user
            })
            res.send(users)
          })
      },

      userInfo: async function(req, res) {
        let userId = req.params.id
        let user = await Model.User.fetch({ id: userId })
        if(!user.length) return res.send({})
        user = user[0]
        if(!user.is_staff) {
          const wallet = await Model.UserWallet.fetch({ user_id: user.id })
          user['walletBalance'] = wallet.length ? wallet[0].balance : 0
        }
        delete user['password']
        res.send(user)
      },

      initalizeWalletTransaction: function(req, res) {
        const email = req.body.email
        const amount = req.body.amount
        if(!email || !amount) return res.status(400).send('Invalid data')

        paystack.initialize(amount, email).then((initData) => {
          res.send(initData)
        }).catch((error) => {
          res.status(400).send(error.message)
        })
      },
      verifyWalletTransaction: async function(req, res) {
        const txId = req.body.txId
        const amount = req.body.amount
        const email = req.body.email
        const description = req.body.description

        if(!txId || !email || (!amount && isNaN(amount))) return res.status(400).send("Invalid data supplied")

        paystack.verify(txId).then(async (verified) => {
          if(verified) {
            let user = await Model.User.get({ email })
            if (!user.length) return res.status(400).send("Invalid User")
            user = user[0]
            const userWallet = await Model.UserWallet.get({ user_id: user.id })
            if (userWallet.length) {
              userWallet[0].balance = Number(userWallet[0].balance) + parseInt(amount / 100)
              await userWallet[0].save()
            } else {
              await Model.UserWallet.create({
                user_id: user.id,
                balance: parseInt(amount / 100)
              })
            }
            res.send("Transaction successful")
          } else {
            res.status(400).send("Invalid payment Transaction")
          }
        })
      },

      contactUs: async function(req, res) {
        if (!req.body.name || !req.body.email || !req.body.message)
        return res.status(400).send('All details must be supplied');
        const contactId = await Model.Contact.create(req.body)
        if (!contactId) return res.status(400).send('Unable to create contact');
        return res.send('Contact Created');
      },

      courseEnroll: async function(req, res) {
        fields = ['name', 'email', 'phone_no', 'program', 'message']
        for (let field in req.body) {
            if (!req.body[field]) return res.status(400).send('Supply Mandatory Fields');
        }
        const enrollId = await Model.Enrollment.create(req.body)
        if (enrollId) res.send("Enrollment Successful!!!")
        else return res.status(400).send('Course Enrollment Failed');
      },

      testGrape: function(req, res) {
        console.log("GRAPE page: " + req.body.page);
      }
};