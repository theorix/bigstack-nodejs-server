const Model = require('../models')

const controller = {
    getContacts: async function (req, res) {
        const contacts = await Model.Contact.fetch()
        res.send(contacts)
    },

    getEnrollments: async function (req, res) {
        const enrolls = await Model.Enrollment.fetch()
        res.send(enrolls)
    },

    addService: async function (req, res) {
        const serviceName = req.body.name
        const description = req.body.description
        if (!serviceName || !description) return res.status(400).send("Supply All fields")
        const serviceId = await Model.Service.create({
            name: serviceName,
            description
        })
        if (serviceId) res.send("New Service added successfully")
        else return res.status(400).send("Error while adding new service")
    },

    editService: async function(req, res) {
        const serviceId = req.params.serviceId;
        const serviceName = req.body.name
        const description = req.body.description
        if (!serviceId || isNaN(serviceId)) return res.status(400).send("Invalid service Id")
        if (!serviceName || !description) return res.status(400).send("Supply All fields")
        const updated = await Model.Service.update({
            set: {
                name: serviceName,
                description
            },
            queryKey: {
                id: serviceId
            }
        })
        if( updated ) {
            res.send("Service updated successfuly")
        } else return res.status(400).send("Failed to update serice")
    },

    removeService: async function(req, res) {
        const serviceId = req.params.serviceId
        if (!serviceId || isNaN(serviceId)) return res.status(400).send("Invalid service Id")
        const deleted = await Model.Service.delete(
            {
                id: serviceId
            }
        )
        if(deleted) res.send("Service updated successfuly")
        else return res.status(400).send("Error deleting service")
    },

    addCourse: async function(req, res) {
        if (!req.body.title || !req.body.description) return res.status(400).send("Supply All fields")
        const courseId = await Model.Course.create(req.body)
        if (courseId) res.send("Service updated successfuly")
        else return res.status(400).send("Error: could not add course")
    },

    editCourse: async function(req, res) {
        const courseId = req.params.courseId;
        const title = req.body.title
        const description = req.body.description
        if (!courseId || isNaN(courseId)) return res.status(400).send("Invalid Id")
        if (!title || !description) return res.status(400).send("Supply All fields")
        const updated = await Model.Course.update({
            set: {
                title,
                description
            },
            queryKey: {
                id: courseId
            }
        })
        if( updated ) {
            res.send("Course updated successfuly")
        } else return res.status(400).send("Failed to update course")
    },

    removeCourse: async function(req, res) {
        const courseId = req.params.courseId
        if (!courseId || isNaN(courseId)) return res.status(400).send("Invalid Id")
        const deleted = await Model.Service.delete(
            {
                id: courseId
            }
        )
        if(deleted) res.send("Course updated successfuly")
        else return res.status(400).send("Error deleting course")
    }
}

module.exports = controller