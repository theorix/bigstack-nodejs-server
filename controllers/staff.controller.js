const Model = require('../models')
const dateUtility = require('../lib/dateUtility')
const qrcodeUtility = require('../lib/qrcode')
const config = require('../config')
const { dateStringFormat } = require('../lib/dateUtility')

function isCorrectLocation(location) {
    if (!location || !location.longitude || !location.latitude) return false
    if (isNaN(location.longitude) || isNaN(location.latitude)) return false
    let locate = config.location
    const long = (location.longitude >= locate.longitude - locate.errorLimit) && (location.longitude <= locate.longitude + locate.errorLimit)
    const lat = (location.latitude >= locate.latitude - locate.errorLimit) && (location.latitude <= locate.latitude + locate.errorLimit)
    // console.log(locate, location, long, lat)
    // console.log(locate.longitude + locate.errorLimit, locate.longitude - locate.errorLimit)
    // console.log(locate.latitude + locate.errorLimit, locate.latitude - locate.errorLimit)
    return long && lat
}

const controller = {
    onboard: async function(req, res) {
        const userId = req.body.userId
        const staffId = req.body.staffId
        const designation = req.body.designation
        const phoneNumber = req.body.phone
        const houseAddress = req.body.houseAddress

        if (!userId || isNaN(userId)) return res.status(400).send("Invalid userId")
        if (!staffId) return res.status(400).send("Invalid staff Id")
        if (!phoneNumber) return res.status(400).send("Invalid phone number")
        if (!houseAddress) return res.status(400).send("Invalid house Address")

        Model.User.getById(userId).then((user) => {
            if (user) {
                Model.Staff.create({
                    user_id: userId,
                    staff_id: staffId,
                    designation,
                    phone: phoneNumber,
                    house_address: houseAddress,
                    salary_details: JSON.stringify({
                        deductions: [],
                        additions: []
                    })
                }).then(async (id) => {
                    if (id !== false) {
                        user.is_staff = 1
                        await user.save()
                        res.send("Staff onboarding successful")
                    }
                    else res.status(400).send("Staff Onboarding failed")
                }).catch((err) => {
                    res.status(400).send(err.message)
                })
            }
        }).catch((error) => {
            res.status(400).send(error.message)
        })

    },

    modifyPaySlip: async function(req, res) {
        const staffId = req.body.staffId
        const isDeduction = req.body.isDeduction
        const detail = req.body.detail
        if (!staffId) return res.status(400).send("Invalid staff Id")
        if (!detail || isNaN(detail.amount) || !detail.description) return res.status(400).send("Invalid detail")

        const staff = await Model.Staff.getById(staffId)
        if(staff) {
            console.log(typeof staff.salary_details)

            const salaryDetails = JSON.parse(staff.salary_details)
            /*
                salaryDetails is of the form: {
                    deductions: [
                        {
                            amount: 500
                            date: '2020-12-14',
                            description: 'Lateness to work'
                        },
                        {
                            amount: 1000
                            date: '2020-10-06',
                            description: 'Absent from work'
                        }
                    ],
                    additions: [
                        {
                            amount: 1200
                            date: '2020-07-01',
                            description: 'Support for data subscription'
                        }
                    ]
                }
            */
           if (isDeduction) {
               salaryDetails.deductions.push(detail)
           } else salaryDetails.additions.push(detail)
           staff.salary_details = JSON.stringify(salaryDetails)
           staff.save().then((success) => {
               console.log("Success")
               if (success)
                res.send('Payslip modified successfully')
               else res.status(400).send('Failed to modify payslip')
           }).catch((error) => {
               console.log("Error: ", error.message)
               res.status(400).send("Payslip could not be modified")
           })
        } else res.status(400).send('Invalid staff')
    },

    generatePayroll: async function(req, res) {
        const staffQuery = `SELECT users.name, staffs.staff_id, staffs.salary, staffs.salary_details FROM users
            INNER JOIN staffs ON users.id = staffs.user_id`;
        const staffs = await Model.User.Query(staffQuery)
        payroll = staffs.results.map((staff) => {
            staff.netSalary = Number(staff.salary)
            const salaryDetails = JSON.parse(staff.salary_details)
            let additions = deductions = {};
            if (salaryDetails.additions.length) 
                additions = salaryDetails.additions.reduce((prev, current) =>{ 
                    prev.amount += current.amount; return prev })
                staff.netSalary += additions.amount
            if (salaryDetails.deductions.length) {
                deductions = salaryDetails.deductions.reduce((prev, current) =>{ 
                    prev.amount += current.amount; return prev })
                staff.netSalary -= deductions.amount
            }
            console.log('staff:', staff)
            return staff
        })

        // const payrollDate = dateUtility.dateStringFormat(new Date())
        const p = await Model.StaffPayroll.get({ approved: false })
        if (p.length) {
            await Model.StaffPayroll.update({
                set: {
                    payroll: JSON.stringify(payroll)
                },
                queryKey: {
                    id: p.id
                }
            })
        } else {
            await Model.StaffPayroll.create({
                payroll: JSON.stringify(payroll),
                payroll_date: payrollDate
            })
        }

        res.send(payroll)
    },

    listPayroll: async function(req, res) {
        Model.StaffPayroll.fetch({ $ext: {
            $orderby: {
                key: 'id',
                order: 'DESC'
            }
        }}).then((payrollList) => {
            res.send(payrollList)
        })
    },

    approvePayroll: async function(req, res) {
        const payrollId = req.body.payrollId
        const staffId = req.body.staffId
        if (!payrollId || isNaN(payrollId)) return res.status(400).send('Invalid payrollId')
        if (!staffId) return res.status(400).send('Invalid staffId')

        let staff = await Model.Staff.get({ staff_id: staffId })
        let payroll = await Model.StaffPayroll.get({ id: payrollId })
        staff = staff[0]
        payroll = payroll[0]

        if(payroll) {
            if (staff.designation.toUpperCase().includes('CEO')) {
                payroll.approved = true
                await payroll.save()
                res.send('Payroll Approved')
            } else res.status(400).send('Unauthorize access to payroll')
        } else res.status(400).send('Invalid payroll')
    },

    createVoucher: async function(req, res) {
        const voucherDataList = req.body.vouchers
        const staffId = req.body.staffId
        const date = req.body.date ? dateUtility.dateStringFormat(req.body.date) : dateUtility.dateStringFormat(new Date())

        if (!staffId) return res.status(400).send('Invalid staff Id')

        let vouchers = []
        for(let voucherData of voucherDataList) {
            if (voucherData.amount && !isNaN(voucherData.amount) && voucherData.description) {
                let voucherId = await Model.Voucher.create({
                    amount: voucherData.amount,
                    description: voucherData.description,
                    staff_id: staffId,
                    voucher_date: date
                })
                if (voucherId) {
                    voucherData['voucherNumber'] = voucherId
                    vouchers.push(voucherData)
                } else {
                    vouchers.push(voucherData)
                }
            }
        }

        res.send(vouchers)
    },

    approveVoucher: async function(req, res) {
        const voucherId = req.body.voucherId
        const staffId = req.body.staffId
        if (!voucherId || isNaN(voucherId)) return res.status(400).send('Invalid voucherId')
        if (!staffId) return res.status(400).send('Invalid staffId')

        let staff = await Model.Staff.get({ staff_id: staffId })
        let voucher = await Model.Voucher.get({ id: voucherId })
        staff = staff[0]
        voucher = voucher[0]

        if(staff) {
            if (staff.designation.toUpperCase().includes('CEO')) {
                voucher.ceo_approved = true
                await voucher.save()
            } else if (staff.designation.includes('Finance')) {
                voucher.finance_approved = true
                await voucher.save()
            }
            res.send('Voucher approved')
        } else {
            res.status(400).send('Unknown staff')
        }
    },

    createIncome: function(req, res) {
        const staffId = req.body.staffId
        const amount = req.body.amount
        const description = req.body.description

        if (!amount || isNaN(amount)) return res.status(400).send('Invalid amount')
        if (!description) return res.status(400).send('Enter a valid description for voucher')
        if (!staffId) return res.status(400).send('Invalid staff Id')

        Model.Income.create({
            staff_id: staffId,
            amount,
            description,
            transaction_date: dateUtility.dateStringFormat(new Date())
        }).then((incomeId) => {
            if(incomeId) res.send("Income added successfully")
            else res.status(400).send("Unable to add income")
        }).catch((error) => {
            res.status(400).send(error.message)
        })

    },

    financialSummary: async function(req, res) {
        const summType = req.params.type
        const summId = req.params.id
        let search_term = req.query.search_term
        const startDate = req.query.start_date ? 
                dateUtility.dateStringFormat(req.query.start_date) : '2019-01-01';
        let endDate = req.query.end_date ? 
                      dateUtility.dateStringFormat(req.query.end_date) : 
                      dateUtility.dateStringFormat(new Date());
                      
        
        if (summType === 'income') {
            let incomeQuery = `SELECT income.id, income.staff_id, income.description, income.amount, 
                                income.transaction_date, users.name FROM income
                                INNER JOIN staffs ON staffs.staff_id = income.staff_id
                                INNER JOIN users ON users.id = staffs.user_id `
            if(summId) {
                const income = await Model.Income.Query(`${incomeQuery} WHERE income.id = "${summId}"`)
                res.send(income.results)
            } else {
                if(search_term) {
                    search_term = `income.description LIKE "%${search_term}%" AND `
                } else search_term = ''
                const incomeList = await Model.Income.Query(`${incomeQuery} WHERE ${search_term}
                        income.transaction_date >= "${startDate}" AND income.transaction_date <= "${endDate}" ORDER BY income.id DESC`)
                res.send(incomeList.results)
            }
            
        } else if (summType === 'expenditure') {
            let voucherQuery = `SELECT voucher.id, voucher.staff_id, voucher.description, voucher.amount, 
                                voucher.voucher_date, voucher.ceo_approved, voucher.finance_approved, users.name FROM voucher
                                INNER JOIN staffs ON staffs.staff_id = voucher.staff_id
                                INNER JOIN users ON users.id = staffs.user_id `
            if(summId) {
                const expenditure = await Model.Voucher.Query(`${voucherQuery} WHERE voucher.id = "${summId}"`)
                res.send(expenditure.results)
            } else {
                if(search_term) {
                    search_term = `voucher.description LIKE "%${search_term}%" AND `
                } else search_term = ''
                const expenditureList = await Model.Income.Query(`${voucherQuery} WHERE ${search_term}
                        voucher.voucher_date >= "${startDate}" AND voucher.voucher_date <= "${endDate}" ORDER BY voucher.id DESC`)
                res.send(expenditureList.results)
            }
        } else { // do both
            res.send([])
        }
    },

    staffCheck: async function(req, res) {
        const staffId = req.body.staffId
        const signInType = req.params.type

        if (!staffId) return res.status(400).send('Invalid staff id')
        if (!isCorrectLocation(req.body.location)) return res.status(400).send('Invalid location')

        const today = dateUtility.dateStringFormat(new Date())
        let signInToday = await Model.StaffSignIn.get({ sign_in_date: today })
        signInToday = signInToday[0]

        if (signInType === 'in') {
            if (!signInToday) {
                const signInTime = new Date().toLocaleTimeString()
                await Model.StaffSignIn.create({
                    staff_id: staffId,
                    sign_in: true,
                    sign_in_date: today,
                    sign_in_time: signInTime
                })
                res.send('Signed in successfully')
            } else {
                res.status(400).send('Already signed in')
            }

        } else if(signInType === 'out') {
            if (!signInToday) {
                res.status(400).send('You must be signed in')
            } else {
                const signOutTime = new Date().toLocaleTimeString()
                signInToday.set('sign_out', true)
                signInToday.set('sign_out_time', signOutTime)
                await signInToday.save()
                res.send('Signed out successfully')
            }
        } else {
            res.status(400).send('Invalid request')
        }
    },

    generateQr: async function(req, res) {
        const startDate = req.query.start_date
        const endDate = req.query.end_date
        if (!startDate || !endDate) res.status(400).send('Invalid date')
        if (dateUtility.dateStringFormat(startDate) < dateUtility.dateStringFormat(new Date()))
            return res.status(400).send('Invalid start date')
        const numberOfDays = dateUtility.dateDiffs(startDate, endDate)
        let qrCodes = []
        for(let n = 0; n < numberOfDays; n++) {
            let day = dateUtility.addDays(startDate, n)
            if(dateUtility.workDay(day)) {
                let date = dateUtility.dateStringFormat(day)
                let signInQrcode = await qrcodeUtility.generate(`BIGSTACK TECHNOLOGIES, SIGN IN: ${date}`)
                let signOutQrcode = await qrcodeUtility.generate(`BIGSTACK TECHNOLOGIES, SIGN OUT: ${date},`)
                qrCodes.push({
                    date,
                    qrcode: {
                        sign_in: signInQrcode,
                        sign_out: signOutQrcode
                    }
                })
            }
        }

        res.send(qrCodes)
    },

    fundUserWallet: async function(req, res) {
        const userEmail = req.body.user_email
        const amount = req.body.amount
        const staffId = req.body.staffId
        if(!amount || isNaN(amount)) return res.status(400).send("Invalid Amount")
        if(!userEmail) return res.status(400).send("User Email can't be empty")
        if(!staffId) return res.status(400).send("Unathorize access")

        const staff = await Model.Staff.fetch({ staff_id: staffId })
        if (!staff.length || 
            (!staff[0].designation.includes('Finance') && 
            !staff[0].designation.includes('CEO') && 
            !staff[0].designation.includes('CTO'))) return res.status(400).send("Unathorize access")

        const user = await Model.User.fetch({ email: userEmail })
        if(user.length) {
            const wallet = await Model.UserWallet.get({ user_id: user[0].id })
            if (!wallet.length) {
                const walletId = await Model.UserWallet.create({
                    user_id: user[0].id,
                    balance: amount
                })
                if(walletId) res.send("Transaction Successful")
                else res.status(400).send("Transaction Failed")
            } else {
                wallet[0].balance = Number(wallet[0].balance) + Number(amount)
                await wallet[0].save()
                res.send("Transaction Successfull")
            }
        } else {
            res.status(400).send("Invalid User")
        }
    },

    profile: async function(req, res) {
        const userId = req.params.id
        if(!userId || isNaN(userId)) return res.status(400).send("Invalid userId")
        const query = `SELECT users.name, users.email, users.is_staff, 
                        staffs.staff_id, staffs.designation, staffs.salary, staffs.salary_details,
                        staffs.phone, staffs.house_address, staffs.account_number FROM users
                        INNER JOIN staffs ON staffs.user_id = users.id
                        WHERE users.id = "${userId}"`
        const staffs = await Model.Staff.Query(query)
        console.log(staffs)
        res.send(staffs.results)
        
    },

    editProfile: async function(req, res) {
        const userId = req.params.id
        if(!userId || isNaN(userId)) return res.status(400).send("Invalid userId")
        const fullName = req.body.name
        const userEmail = req.body.email
        const staffId = req.body.staff_id
        const staffDesignation = req.body.designation
        const staffSalary = req.body.salary
        const staffPhone = req.body.phone
        const staffHouseAddr = req.body.house_address
        const staffAccountNum = req.body.account_number

        let user = await Model.User.get({ id: userId })
        let staff = await Model.Staff.get({ user_id: userId, staff_id: staffId })
        if (!user.length) return res.status(400).send("Invalid user")
        if (!staff.length) return res.status(400).send("Invalid Staff")
        if (!userEmail || !fullName) return res.status(400).send("Invalid Staff")
        user = user[0]
        staff = staff[0]

        user.name = fullName
        user.email = userEmail

        staff.staff_id = staffId
        staff.designation = staffDesignation
        staff.salary = staffSalary
        staff.phone = staffPhone
        staff.house_address = staffHouseAddr
        staff.account_number = staffAccountNum

        await user.save()
        await staff.save()

        res.send("Success")
    }
}

module.exports = controller