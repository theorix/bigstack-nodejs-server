const Model = require("../models")
const dateUtility = require('../lib/dateUtility')
const hubUtility = require('../lib/hubUtility')
const config = require('../config')

const HubBooking = Model.HubBooking;


// get hub info
const hubInfo = (req, res) => {
	res.send(config.hubWorkSpace)
}

// book a hub space
const book = async (req, res) => {
	// check for invalid date
	if (!req.body.start_date || dateUtility.dateStringFormat(req.body.start_date) < dateUtility.dateStringFormat(new Date())) {
		return res.send('Invalid StartDate')
	}

	// TODO: convert duration to days so as to determine end date
	const hubPlan = req.body.hub_plan
	const duration = req.body.duration
	const startDate = dateUtility.dateStringFormat(req.body.start_date)
	let nDays = 0
	let numberOfDaysInAWeek = config.hubWorkSpace.numberOfDaysInAWeek

	// decode number of days
	let durationType = req.body.duration.type
	if (durationType) durationType = durationType.toUpperCase()
	if (durationType === 'DAY' || durationType === 'DAYS') {
		nDays = duration.duration
	} else if (durationType === 'WEEK' || durationType === 'WEEKS') {
		nDays = numberOfDaysInAWeek * duration.duration
	} else if (durationType === 'MONTH' || durationType === 'MONTHS') {
		nDays = 4 * numberOfDaysInAWeek * duration.duration
	} else if (durationType === 'YEAR' || durationType === 'YEARS') {
		nDays = 4 * numberOfDaysInAWeek * 12 * duration.duration
	} else return res.send('Invalid duration type')

	const endDate = dateUtility.dateStringFormat(dateUtility.addDays(req.body.start_date, nDays - 1))
	const data = {
		email: req.body.email,
		hub_package: `${hubPlan.name} (${hubPlan.description})`,
		price: hubPlan.price,
		start_date: startDate,
		end_date: endDate,
		full_name: !req.body.full_name ? '-' : req.body.full_name,
		payment_reference: !req.body.payment_reference ? '-' : req.body.payment_reference,
		ignore_wallet: req.body.ignore_wallet
	}

	hubUtility.bookWorkspace(data).then(async (bookingJson) => {
		await Model.Income.create({
			staff_id: `BST/USER/${bookingJson.email}`,
			amount: bookingJson.totalAmount,
			description: `${hubPlan.name} (${hubPlan.description})`,
			transaction_date: dateUtility.dateStringFormat(new Date())
		  })
		res.send(bookingJson)
	}).catch((e) => {
		console.log("Error booking:", e.message)
		res.status(400).send(e.message)
	})
}

// check in
const checkIn = async (req, res) => {
	const userEmail = req.body.email
	const staffId = req.body.staffId
	if(!userEmail) return reject(new Error('Invalid User Email'))
	if(!staffId || isNaN(staffId)) return reject(new Error('Inavlid Staff Id'))
	hubUtility.checkIn(userEmail, staffId)
	 .then((bookingDetail) => {
		 res.send(bookingDetail)
	 }).catch((e)=> {
		 res.headers(400).send(e.message)
	 })
}

// get all registered customers
const listBookings = (req, res) => {
	HubBooking.fetch()
		.then(result => {
			res.json(result);
		})
		.catch(err => {
			console.log(err);
		})
}

//delete a registered customer
const deleteBookings = (req, res) => {
	HubBooking.delete({ id: req.params.id })
		.then(deleted => {
			if (deleted)
				res.send("booking deleted successfully");
			else res.headers(400).send(`Could not delete booking: ${req.params.id}`)
		})
		.catch(err => {
			console.log(err);
		});
}

//get all active bookings
const getActiveBookings = (req, res) => {
	HubBooking.fetch({ subscription_status: "Active" })
		.then(bookings => {
			res.json(bookings);
		})
		.catch(err => console.log(err));
}

module.exports = {
	hubInfo,
	book,
	checkIn,
	listBookings,
	deleteBookings,
	getActiveBookings
}