const staff = require("../models/staff");

const express            = require("express"),
	  staffController = require("../controllers/staff.controller"),
	  router             = express.Router();

router.post("/onboard", staffController.onboard);
router.post("/payslip", staffController.modifyPaySlip);
router.get("/payroll", staffController.generatePayroll)
router.get("/payroll/list", staffController.listPayroll)
router.post("/payroll/approve", staffController.approvePayroll)
router.post("/voucher", staffController.createVoucher)
router.post("/voucher/approve", staffController.approveVoucher)
router.post("/income", staffController.createIncome)
router.get("/finance/summary/:type/:id?", staffController.financialSummary)
router.post("/sign/:type", staffController.staffCheck)
router.get("/qrcode", staffController.generateQr)
router.post("/fund/user/wallet", staffController.fundUserWallet)
router.get("/profile/:id", staffController.profile)
router.post("/profile/:id", staffController.editProfile)
module.exports = router;