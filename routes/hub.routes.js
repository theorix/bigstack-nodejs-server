const express            = require("express"),
	  bookingController = require("../controllers/hub.controller"),
	  router             = express.Router();

router.post("/book", bookingController.book);
router.get("/", bookingController.listBookings);
router.get("/active", bookingController.getActiveBookings);
router.delete("/:id", bookingController.deleteBookings);
router.post("/checkin", bookingController.checkIn);
router.get("/status", bookingController.hubInfo)

module.exports = router;