const express            = require("express"),
	  userController = require("../controllers/user.controller"),
	  router             = express.Router();

router.post("/signup", userController.register);
router.post("/login", userController.login);
router.post("/transaction/init", userController.initalizeWalletTransaction)
router.post("/transaction/verify", userController.verifyWalletTransaction)
router.post("/contact", userController.contactUs)
router.post("/enroll", userController.courseEnroll)
router.get('/list', userController.listUsers)
router.get("/info/:id", userController.userInfo)
router.post('/grape/test', userController.testGrape)

module.exports = router;