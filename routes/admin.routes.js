const express            = require("express"),
	  adminController = require("../controllers/admin.controller"),
	  router             = express.Router();

router.get("/contacts", adminController.getContacts);
router.get("/enroll", adminController.getEnrollments);
router.post("/services", adminController.addService)

module.exports = router;