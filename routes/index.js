const bookingRoutes = require("./hub.routes"),
    userRoutes = require("./user.routes"),
    staffRoutes = require("./staff.routes"),
    adminRoutes = require("./admin.routes");

module.exports = function(app) {
    app.use('/user', userRoutes)
    app.use('/staff', staffRoutes)
    app.use("/hub", bookingRoutes);
    app.use('/admin', adminRoutes)
}