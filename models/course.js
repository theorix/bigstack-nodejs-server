
module.exports = {
    config: {
      tableName: 'courses',
      primaryKey: 'id'
    },
    fields: [
      'id', 'title', 'description', 'created_at'
    ]
  }