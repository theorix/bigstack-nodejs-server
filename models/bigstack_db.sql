-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 18, 2021 at 01:00 PM
-- Server version: 8.0.22-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bigstack_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `message`, `created_at`, `updated_at`) VALUES
(1, 'The name', 'email@me.com', 'Contact me here', NULL, NULL),
(2, 'Another name', 'eeell@gmail.com', 'You me here', '2020-12-04 12:28:50', '2020-12-04 12:28:50');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` bigint UNSIGNED NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `enrollments`
--

CREATE TABLE `enrollments` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone_no` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `training_program` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `referral` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `enrollments`
--

INSERT INTO `enrollments` (`id`, `name`, `email`, `phone_no`, `training_program`, `message`, `created_at`, `updated_at`, `referral`) VALUES
(1, 'New Name', 'myemail@gmail.com', '094938404', 'Web Design', 'Message to the admin', NULL, NULL, NULL),
(2, 'Antoher New Name', 'gmelael@gmail.com', '094938404', 'Graphic design', 'Message to the admin', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `connection` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hub_bookings`
--

CREATE TABLE `hub_bookings` (
  `id` int NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `hub_package` varchar(50) NOT NULL,
  `payment_reference` varchar(255) NOT NULL,
  `subscription_status` varchar(15) NOT NULL DEFAULT 'Pending' COMMENT 'Pending, Active, Expired'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hub_bookings`
--

INSERT INTO `hub_bookings` (`id`, `full_name`, `email`, `start_date`, `end_date`, `hub_package`, `payment_reference`, `subscription_status`) VALUES
(9, 'Bala Caleb', 'caleb@gmail.com', '2020-12-08', '2020-12-10', 'daily', '856pay123', 'Active'),
(11, 'Bala Caleb', 'caleb@gmail.com', '2020-12-31', '2021-01-10', 'daily', '856pay123', 'Active'),
(12, 'Bala Caleb', 'caleb@gmail.com', '2021-01-05', '2021-01-21', 'daily', '856pay123', 'Active'),
(49, '-', 'caleb2@gmail.com', '2020-12-23', '2021-01-01', 'Basic (Workspace Only)', '-', 'Pending'),
(50, '-', 'caleb2@gmail.com', '2020-12-23', '2020-12-25', 'Basic (Workspace Only)', '-', 'Pending'),
(57, '-', 'caleb2@gmail.com', '2021-01-13', '2021-01-14', 'Basic (Workspace Only)', '-', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `hub_checkins`
--

CREATE TABLE `hub_checkins` (
  `id` int NOT NULL,
  `booking_id` int NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `income`
--

CREATE TABLE `income` (
  `id` int NOT NULL,
  `staff_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `description` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `amount` int NOT NULL,
  `transaction_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `income`
--

INSERT INTO `income` (`id`, `staff_id`, `description`, `amount`, `transaction_date`) VALUES
(1, 'BST/CRS/009', 'Three months web design training fee for Mr. John Doe', 35000, '2020-12-29 23:00:00'),
(2, 'BST/USER/caleb2@gmail.com', 'Basic (Workspace Only)', 700, '2021-01-12 23:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_05_14_102813_create_posts_table', 1),
(4, '2020_05_14_122817_create_comments_table', 1),
(5, '2020_05_20_101139_create_media_table', 1),
(6, '2020_05_22_160541_create_post_subscribers_table', 1),
(7, '2020_06_04_125916_contact_table', 1),
(8, '2020_06_18_130332_enrollment_form', 1),
(9, '2020_06_22_120620_update_enrollment_form_referral', 1),
(10, '2020_06_30_125918_add_html_column_to_posts', 1),
(11, '2020_07_01_125252_services', 1),
(12, '2020_07_03_111058_add_state_to_posts', 1),
(13, '2020_07_15_112640_clear_media_columns', 1),
(14, '2020_07_28_131949_service_add_timestamp', 1),
(15, '2020_08_14_101248_course', 1),
(16, '2020_08_14_101358_topic', 1),
(17, '2020_08_14_101433_video', 1),
(18, '2020_09_08_143331_video_comments', 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint UNSIGNED NOT NULL,
  `html_description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `staffs`
--

CREATE TABLE `staffs` (
  `user_id` int NOT NULL,
  `staff_id` varchar(25) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `designation` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL COMMENT 'CEO, CTO, CMO, etc',
  `salary` int NOT NULL DEFAULT '0',
  `salary_details` varchar(300) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `account_number` varchar(11) DEFAULT NULL,
  `phone` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `house_address` varchar(200) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staffs`
--

INSERT INTO `staffs` (`user_id`, `staff_id`, `designation`, `salary`, `salary_details`, `account_number`, `phone`, `house_address`) VALUES
(9, 'BST/CRS/001', 'Chief Executive Officer (CEO)', 0, '{\"deductions\":[],\"additions\":[]}', NULL, '08035867476', '#131 goldie street calabar'),
(1, 'BST/CRS/009', 'Senior Back-end Dev.', 30000, '{\"deductions\":[{\"amount\":1300,\"description\":\"Payment for lateness to work\",\"date\":\"2020-06-19\"}],\"additions\":[{\"amount\":8000,\"description\":\"Data support for the month\",\"date\":\"2020-06-19\"},{\"amount\":8000,\"description\":\"Data support for the month\",\"date\":\"2020-06-19\"}]}', NULL, '08035867476', '#131 goldie street calabar');

-- --------------------------------------------------------

--
-- Table structure for table `staff_payroll`
--

CREATE TABLE `staff_payroll` (
  `id` int NOT NULL,
  `payroll` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `approved` tinyint NOT NULL DEFAULT '0',
  `payroll_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_payroll`
--

INSERT INTO `staff_payroll` (`id`, `payroll`, `approved`, `payroll_date`) VALUES
(4, '[{\"name\":\"Erim Saviour\",\"staff_id\":\"BST/CRS/009\",\"salary\":30000,\"salary_details\":\"{\\\"deductions\\\":[{\\\"amount\\\":1300,\\\"description\\\":\\\"Payment for lateness to work\\\",\\\"date\\\":\\\"2020-06-19\\\"}],\\\"additions\\\":[{\\\"amount\\\":8000,\\\"description\\\":\\\"Data support for the month\\\",\\\"date\\\":\\\"2020-06-19\\\"},{\\\"amount\\\":8000,\\\"description\\\":\\\"Data support for the month\\\",\\\"date\\\":\\\"2020-06-19\\\"}]}\",\"netSalary\":44700}]', 1, '2020-12-27');

-- --------------------------------------------------------

--
-- Table structure for table `staff_sign_in`
--

CREATE TABLE `staff_sign_in` (
  `id` int NOT NULL,
  `staff_id` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sign_in_date` date NOT NULL,
  `sign_in` tinyint(1) NOT NULL DEFAULT '0',
  `sign_out` tinyint(1) NOT NULL DEFAULT '0',
  `sign_in_time` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sign_out_time` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_sign_in`
--

INSERT INTO `staff_sign_in` (`id`, `staff_id`, `sign_in_date`, `sign_in`, `sign_out`, `sign_in_time`, `sign_out_time`) VALUES
(4, 'BST/CRS/009', '2020-12-30', 1, 1, '9:11:54 AM', '9:13:20 AM'),
(5, 'BST/CRS/009', '2021-01-16', 1, 0, '2:18:42 PM', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `topic`
--

CREATE TABLE `topic` (
  `id` bigint UNSIGNED NOT NULL,
  `course_id` int UNSIGNED NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL DEFAULT '0',
  `email` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `is_staff`, `email`, `password`) VALUES
(1, 'Erim Saviour', 1, 'bigstack.official@gmail.com', '$2y$10$vQsKQnbPQF23y2TB55YL2O6h6Sx..yjr41WIP8OebRimny0j/2iz.'),
(2, 'Mosi', 1, 'okimmosimiracle@gmail.com', '$2y$10$4x24VB.w4Fx2gk6GFVbEd.WauHdhfm7GI6tPjLa0BwK2EcK3xGbTa'),
(3, 'Victoria', 0, 'vickypatrick77@gmail.com', '$2y$10$..0D8My5cZJirUbc22jdnuW6vJlatAIvG0b68E59YBozarpzwZmBy'),
(4, 'KonyeKokim Odey', 0, 'Konye@gmail.com', '$2y$10$J7DYWBANs6hlA18q3Atwuu.RIK924R/eK0.y4iZEVW8mK5joZHwhy'),
(7, 'Bala Caleb', 0, 'caleb@gmail.com', '$2b$10$5JbYTSjfuxesr7gi9rmhp.YmjFP.mnCOjQAC4tCXwE/Nhh1i4A7rm'),
(8, 'ytrty', 0, 'abc@gmail.com', '$2b$10$3C2V6hs1quLD.KZHjb8R..4p4P894K07r0OqHmz1z7wcVIdDairJa'),
(9, 'Agba Thomas', 1, 'agbatom@gmail.com', '$2b$10$LIH8mqIjFff46KFtSCr4Su09vP5pNRTka2Rt5oXllyd/wVBLzVVGa');

-- --------------------------------------------------------

--
-- Table structure for table `users_wallet`
--

CREATE TABLE `users_wallet` (
  `id` int NOT NULL,
  `user_id` int NOT NULL,
  `balance` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_wallet`
--

INSERT INTO `users_wallet` (`id`, `user_id`, `balance`) VALUES
(1, 8, 2000);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` bigint UNSIGNED NOT NULL,
  `topic_id` int UNSIGNED NOT NULL,
  `url` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `videocomments`
--

CREATE TABLE `videocomments` (
  `id` bigint UNSIGNED NOT NULL,
  `video_id` int UNSIGNED DEFAULT NULL,
  `email` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'anonymous',
  `name` varchar(191) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'anonymous',
  `body` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `voucher`
--

CREATE TABLE `voucher` (
  `id` int NOT NULL,
  `amount` int NOT NULL,
  `staff_id` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `finance_approved` tinyint(1) NOT NULL DEFAULT '0',
  `ceo_approved` tinyint(1) NOT NULL DEFAULT '0',
  `voucher_date` date NOT NULL,
  `description` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voucher`
--

INSERT INTO `voucher` (`id`, `amount`, `staff_id`, `finance_approved`, `ceo_approved`, `voucher_date`, `description`) VALUES
(1, 5000, 'BST/CRS/009', 0, 1, '2020-12-30', 'Purchase of Covid-19 utilities'),
(2, 9000, 'BST/CRS/009', 0, 0, '2021-01-07', 'Purchase of Electrical appliances'),
(3, 4300, 'BST/CRS/009', 0, 0, '2021-01-07', 'Payment of monthly dues'),
(4, 3000, 'BST/CRS/009', 0, 0, '2021-01-07', 'Purchase of detergents');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrollments`
--
ALTER TABLE `enrollments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hub_bookings`
--
ALTER TABLE `hub_bookings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `income`
--
ALTER TABLE `income`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staffs`
--
ALTER TABLE `staffs`
  ADD PRIMARY KEY (`staff_id`),
  ADD UNIQUE KEY `staff_id` (`staff_id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indexes for table `staff_payroll`
--
ALTER TABLE `staff_payroll`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_sign_in`
--
ALTER TABLE `staff_sign_in`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topic`
--
ALTER TABLE `topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `users_wallet`
--
ALTER TABLE `users_wallet`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `videocomments`
--
ALTER TABLE `videocomments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `voucher`
--
ALTER TABLE `voucher`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enrollments`
--
ALTER TABLE `enrollments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `hub_bookings`
--
ALTER TABLE `hub_bookings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `income`
--
ALTER TABLE `income`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_payroll`
--
ALTER TABLE `staff_payroll`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `staff_sign_in`
--
ALTER TABLE `staff_sign_in`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `topic`
--
ALTER TABLE `topic`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users_wallet`
--
ALTER TABLE `users_wallet`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `videocomments`
--
ALTER TABLE `videocomments`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `voucher`
--
ALTER TABLE `voucher`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
