
module.exports = {
    config: {
      tableName: 'income',
      primaryKey: 'id'
    },
    fields: [
      'id', 'staff_id', 'description', 'amount', 'transaction_date'
    ]
  }