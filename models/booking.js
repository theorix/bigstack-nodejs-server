
module.exports = {
    config: {
      tableName: 'hub_bookings',
      primaryKey: 'id'
    },
    fields: [
      'id', 'full_name', 'email', 'start_date', 'end_date', 'hub_package', 'payment_reference', 'subscription_status'
    ]
  }