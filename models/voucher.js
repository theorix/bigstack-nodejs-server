
module.exports = {
    config: {
      tableName: 'voucher',
      primaryKey: 'id'
    },
    fields: [
      'id', 'staff_id', 'description', 'amount', 'voucher_date', 'finance_approved', 'ceo_approved'
    ]
  }