
module.exports = {
    config: {
      tableName: 'hub_checkins',
      primaryKey: 'id'
    },
    fields: [
      'id', 'booking_id', 'created_at'
    ]
  }