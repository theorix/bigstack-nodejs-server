
module.exports = {
    config: {
      tableName: 'enrollments',
      primaryKey: 'id'
    },
    fields: [
      'id', 'name', 'email', 'phone_no', 'training_program', 'message', 'referral', 'created_at'
    ]
  }