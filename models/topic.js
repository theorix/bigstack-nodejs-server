
module.exports = {
    config: {
      tableName: 'topic',
      primaryKey: 'id'
    },
    fields: [
      'id', 'course_id', 'description', 'title', 'created_at', 'updated_at'
    ]
  }