
module.exports = {
    config: {
      tableName: 'staffs',
      primaryKey: 'staff_id'
    },
    fields: [
      'user_id', 'staff_id', 'designation', 'salary', 'salary_details', 'phone', 'house_address'
    ]
  }