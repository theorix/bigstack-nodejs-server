
module.exports = {
    config: {
      tableName: 'staff_payroll',
      primaryKey: 'id'
    },
    fields: [
      'id', 'payroll', 'payroll_date', 'approved'
    ]
  }