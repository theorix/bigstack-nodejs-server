
module.exports = {
    config: {
      tableName: 'contacts',
      primaryKey: 'id'
    },
    fields: [
      'id', 'name', 'email', 'message', 'created_at'
    ]
  }