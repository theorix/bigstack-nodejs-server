const hubBooking = require('./booking'),
  hubCheckin = require('./hubCheckin'),
  user = require('./user'),
  staff = require('./staff'),
  staffPayroll = require('./staffPayroll'),
  staffSignIn = require('./staffSignIn'),
  income = require('./income'),
  voucher = require('./voucher'),
  userWallet = require('./userWallet'),
  course = require('./course'),
  contact = require('./contact'),
  enrollment = require('./enrollment'),
  service = require('./service'),
  topic = require('./topic'),
  video = require('./video'),
  videoComment = require('./videoComment'),
  config = require('../config')


// configure ORM
const duffORM = require('../lib/duff-orm')
duffORM.configure({
  db: {
    driver: 'mysql',
    mysql: {
      host: config.db.host,
      user: config.db.user,
      password: config.db.password,
      database: config.db.database
    }
  },
  debug: false
})

// generate the models using the orm
const Model = duffORM.model()
module.exports = {
  HubBooking: Model(hubBooking),
  hubCheckin: Model(hubCheckin),
  UserWallet: Model(userWallet),
  Staff: Model(staff),
  StaffPayroll: Model(staffPayroll),
  StaffSignIn: Model(staffSignIn),
  Income: Model(income),
  Voucher: Model(voucher),
  User: Model(user),
  Contact: Model(contact),
  Course: Model(course),
  Enrollment: Model(enrollment),
  Service: Model(service),
  Topic: Model(topic),
  Video: Model(video),
  VideoComment: Model(videoComment)
}
