
module.exports = {
    config: {
      tableName: 'videocomments',
      primaryKey: 'id'
    },
    fields: [
      'id', 'video_id', 'email', 'name', 'body', 'created_at', 'updated_at'
    ]
  }