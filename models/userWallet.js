
module.exports = {
    config: {
      tableName: 'users_wallet',
      primaryKey: 'id'
    },
    fields: [
      'id', 'user_id', 'balance'
    ]
  }