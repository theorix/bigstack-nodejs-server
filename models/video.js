
module.exports = {
    config: {
      tableName: 'video',
      primaryKey: 'id'
    },
    fields: [
      'id', 'topic_id', 'url', 'description', 'title', 'created_at', 'updated_at'
    ]
  }