
module.exports = {
    config: {
      tableName: 'services',
      primaryKey: 'id'
    },
    fields: [
      'id', 'name', 'html_description', 'created_at', 'updated_at'
    ]
  }