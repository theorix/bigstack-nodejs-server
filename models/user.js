const config = require('../config');
const jwt = require('jsonwebtoken');

module.exports = {
    config: {
      tableName: 'users',
      primaryKey: 'id'
    },
    fields: [
      'id', 'name', 'email', 'is_staff', 'password'
    ],

    override: {
        classMethods: {
            //custom method to generate authToken 
            generateAuthToken: function() { 
                const token = jwt.sign({ id: this.id, isAdmin: this.is_staff }, config['tokenPrivateKey']); //get the private key from the config file -> environment variable
                return token;
            }
        }
    }
  }