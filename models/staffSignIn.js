
module.exports = {
    config: {
      tableName: 'staff_sign_in',
      primaryKey: 'id'
    },
    fields: [
      'id',  'staff_id', 'sign_in', 'sign_out', 'sign_in_time', 'sign_out_time'
    ]
  }